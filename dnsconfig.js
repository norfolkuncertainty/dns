var GANDI = NewDnsProvider("gandi");
var REG_GANDI = NewRegistrar("gandi");

D("davenull.co.nz", REG_GANDI, DnsProvider(GANDI),
    A("@","114.23.165.201"),
    A("ha","114.23.165.201"),
    ALIAS("*","davenull.co.nz."),
    ALIAS("fm1._domainkey","fm1.davenull.co.nz.dkim.fmhosted.com."),
    ALIAS("fm2._domainkey","fm2.davenull.co.nz.dkim.fmhosted.com."),
    ALIAS("fm3._domainkey","fm3.davenull.co.nz.dkim.fmhosted.com."),
    DMARC_BUILDER({
        policy: 'reject',
        alignmentSPF: 'strict',
        alignmentDKIM: 'strict',
        subdomainPolicy: 'reject',
        rua: [
            'mailto:dmarc@davenull.co.nz'
        ],
        ruf: [
            'mailto:dmarc@davenull.co.nz'
        ],
        failureOptions: '1',
    }),
    SPF_BUILDER({
        label: "@",
        parts: [
            "v=spf1",
            "include:spf.messagingengine.com",
            "-all"
        ],
    }),
    MX("@",10,"in1-smtp.messagingengine.com."),
    MX("@",20,"in2-smtp.messagingengine.com."),
    TXT("*._report._dmarc","v=DMARC1")
);
