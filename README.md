# dns

DNS config for my domains via [dnscontrol](https://github.com/StackExchange/dnscontrol)

## Usage

Encrypt creds.json
```
gpg -ea -r dave@davenull.co.nz creds.json
```

Decrypt creds.json
```
gpg --decrypt creds.json.asc
```
Test Config
```
docker run --rm -it -v $(pwd)/dnsconfig.js:/dns/dnsconfig.js -v $(pwd)/creds.json:/dns/creds.json stackexchange/dnscontrol dnscontrol preview
```

Deploy DNS config
```
docker run --rm -it -v $(pwd)/dnsconfig.js:/dns/dnsconfig.js -v $(pwd)/creds.json:/dns/creds.json stackexchange/dnscontrol dnscontrol push
```

## Authors and acknowledgment

Thanks to [dnscontrol](https://github.com/StackExchange/dnscontrol)
